require 'pathname'

desc 'Installs powerline'
task :powerline do
  sh "pip install --upgrade psutil mercurial pygit2 bzr i3-py"
  sh "pip install --upgrade git+git://github.com/Lokaltog/powerline"
end
