require 'pathname'

desc 'Generates base16 theme files'
task :base16 do
  base16 = Pathname.new('base16-builder/base16')
  sh "#{base16} &> /dev/null" if base16.executable?
end
