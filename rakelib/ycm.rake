require 'pathname'

desc 'Builds YouCompleteMe'
task :ycm do
  clang = Pathname.new('../clang+llvm-3.2-x86_64-apple-darwin11')
  ycm = Pathname.new('../dotvim/bundle/YouCompleteMe/cpp')
  build = Pathname.new('ycm_build')
  build.mkdir unless build.exist?
  cd build
  sh "cmake #{ycm} -DPATH_TO_LLVM_ROOT=#{clang} && make"
end
